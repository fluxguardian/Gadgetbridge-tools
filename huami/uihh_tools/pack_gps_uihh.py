# Copyright (C) 2021 Petr Vaněk
#
# This file is part of Gadgetbridge-tools.
#
# Gadgetbridge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gadgetbridge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import typemap as tm
import sys
from binascii import crc32
import pathlib

if len(sys.argv) < 3:
    print(f"USAGE: {sys.argv[0]} <directory> <outputfile>")
    sys.exit()

inputdir = sys.argv[1]
outputfile = sys.argv[2]

content = b""

for typeID, inputfilename in tm.typemap.items():
    fullPathName = pathlib.Path(inputdir).joinpath(inputfilename)
    if not fullPathName.is_file():
        print(f"[E] File not found: {fullPathName}")
        sys.exit()

    with open(fullPathName, "rb") as f:
        filecontent = f.read()

    print(f"[I] Packing {inputfilename}")
    fileheader = (
        bytes([1])
        + typeID.to_bytes(1, "little")
        + len(filecontent).to_bytes(4, "little")
        + crc32(filecontent).to_bytes(4, "little")
    )
    content += fileheader + filecontent

print("[I] Adding header")
header = (
    b"UIHH"
    + bytes([0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01])
    + crc32(content).to_bytes(4, "little")
    + bytes([0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
    + len(content).to_bytes(4, "little")
    + bytes([0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
)

content = header + content

print(f"[I] Writing {outputfile}")

with open(outputfile, "wb") as f:
    f.write(content)

print("[I] All done.")
