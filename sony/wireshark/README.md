# Wireshark dissector for Sony Headphones protocol

## Devices

- WH-1000XM3 (fw 4.5.2)
- WF-SP800N (fw 1.1.0)
- WF-1000XM4 (not supported, uses new protocol)

## Known issues

- Unescaping not implemented
