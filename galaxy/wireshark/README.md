# Wireshark dissector for Galaxy Buds protocol

Dissector for the Galaxy Buds protocol. Place it into `~/.local/lib/wireshark/plugins` . Apply this by typing `galaxy` into the `display filter` top input line.
